package com.devper.registropedido;

import java.util.Random;

public class Helper
{

    public static long getNewId()
    {
        return Math.abs(new Random(System.currentTimeMillis()).nextLong());
    }
}
