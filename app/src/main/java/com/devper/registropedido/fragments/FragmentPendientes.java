package com.devper.registropedido.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.devper.registropedido.App;
import com.devper.registropedido.activities.Main;
import com.devper.registropedido.R;
import com.devper.registropedido.adapters.PedidosPendientesAdapter;
import com.devper.registropedido.models.Pedido;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class FragmentPendientes extends Fragment implements View.OnClickListener{

    View mViewRoot;
    RecyclerView rvLista;
    ArrayList<Pedido> pedidos;
    PedidosPendientesAdapter adapter;
    ImageButton btnAgregar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewRoot=inflater.inflate(R.layout.fragment_pendientes,container,false);
        rvLista=mViewRoot.findViewById(R.id.rvLista);
        btnAgregar=mViewRoot.findViewById(R.id.btnAgregar);
        btnAgregar.setOnClickListener(this);
        return mViewRoot;
    }

    @Override
    public void onClick(View v) {
        ((Main)getActivity()).mostrarAgregarPedido();

    }

    ListenerRegistration dbListener;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pedidos=new ArrayList<>();

        //se inicializa el adapter con la lista vacia y con referencia al main activity
        adapter=new PedidosPendientesAdapter(pedidos,(Main)getActivity());
        LinearLayoutManager manager=new LinearLayoutManager(getContext());
        rvLista.setLayoutManager(manager);

        rvLista.setAdapter(adapter);

        //aqui preparamos el query para la bd firestore
        query=App.class.cast(getActivity().getApplication())
                .getFbFirestore()
                .collection("pedido")
                .whereEqualTo("mozo_id",App.class.cast(getActivity().getApplication()).getFbAuth().getCurrentUser().getUid())
                //.orderBy("fecha",Query.Direction.ASCENDING)
                ;


    }

    Query query;
    @Override
    public void onResume() {
        super.onResume();




        dbListener= query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {

                //limpiamos la lista
                pedidos.clear();

                //y genreamos una nueva lista objeto por objeto resultado del query
                for(QueryDocumentSnapshot doc:queryDocumentSnapshots)
                {
                    Pedido pedido=new Pedido();
                    pedido.idDoc=doc.getId();
                    pedido.precio=doc.getDouble("precio");
                    pedido.meza_nombre=doc.getString("meza_nombre");
                    pedido.mozo_id=doc.getString("mozo");
                    pedido.mozo_nombre=doc.getString("mozo_nombre");
                    pedido.detalle=doc.getString("detalle");

                    pedidos.add(pedido);

                }
                adapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();



        if(dbListener!=null)
            dbListener.remove();
    }


}
