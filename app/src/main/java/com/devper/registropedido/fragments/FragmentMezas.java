package com.devper.registropedido.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devper.registropedido.App;
import com.devper.registropedido.activities.Main;
import com.devper.registropedido.R;
import com.devper.registropedido.adapters.MezasAdapter;
import com.devper.registropedido.models.Meza;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class FragmentMezas extends Fragment {

    View mViewRoot;
    RecyclerView rvLista;
    ArrayList<Meza> mezas;
    MezasAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewRoot=inflater.inflate(R.layout.fragment_mezas,container,false);
        rvLista=mViewRoot.findViewById(R.id.rvLista);

        return mViewRoot;
    }


    ListenerRegistration dbListener;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mezas=new ArrayList<>();

        adapter=new MezasAdapter(mezas,(Main)getActivity());
        LinearLayoutManager manager=new LinearLayoutManager(getContext());
        rvLista.setLayoutManager(manager);

        rvLista.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        super.onResume();

        dbListener= App.class.cast(getActivity().getApplication()).getFbFirestore().collection("meza")
                //.whereEqualTo("nick","javier")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                        @Nullable FirebaseFirestoreException e)
                    {

                        //queryDocumentSnapshots.size()
                        mezas.clear();

                        Log.v("db","size "+queryDocumentSnapshots.size());
                        for(QueryDocumentSnapshot doc:queryDocumentSnapshots)
                        {
                            Log.v("db ",doc.getId());


                            String nombre=doc.getString("nombre");
                            double costo=doc.getLong("capacidad");

                            Meza meza=new Meza();
                            meza.idMeza=doc.getId();
                            meza.nombre=nombre;
                            meza.capacidad=(int)costo;
                            mezas.add(meza);

                        }
                        adapter.notifyDataSetChanged();

                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();

        if(dbListener!=null)
            dbListener.remove();
    }


}
