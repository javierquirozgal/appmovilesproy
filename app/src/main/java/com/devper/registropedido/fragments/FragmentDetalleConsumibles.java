package com.devper.registropedido.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.devper.registropedido.App;
import com.devper.registropedido.R;
import com.devper.registropedido.adapters.ConsumiblesDetalleAdapter;
import com.devper.registropedido.models.Consumible;
import com.devper.registropedido.models.Pedido;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class FragmentDetalleConsumibles extends Fragment implements View.OnClickListener{


    public Pedido pedido;

    View mViewRoot;
    RecyclerView rvLista;
    ArrayList<Consumible> consumibles;
    ConsumiblesDetalleAdapter adapter;
    ImageButton btnPagado;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewRoot=inflater.inflate(R.layout.fragment_detalleconsumibles,container,false);
        rvLista=mViewRoot.findViewById(R.id.rvLista);

        btnPagado=mViewRoot.findViewById(R.id.btnPagado);
        btnPagado.setOnClickListener(this);
        return mViewRoot;
    }

    @Override
    public void onClick(View v) {

        App.class.cast(getActivity().getApplication()).getFbFirestore()
                .collection("pedido")
                .document(pedido.idDoc)
                .delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getContext(),"Pedido Pagado",Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(),"No se pudo pagar",Toast.LENGTH_SHORT).show();
            }
        });

    }

    ListenerRegistration dbListener;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        consumibles=new ArrayList<>();

        adapter=new ConsumiblesDetalleAdapter(consumibles);
        LinearLayoutManager manager=new LinearLayoutManager(getContext());
        rvLista.setLayoutManager(manager);

        rvLista.setAdapter(adapter);


    }

    @Override
    public void onResume() {
        super.onResume();

        dbListener= App.class.cast(getActivity().getApplication()).getFbFirestore().collection("pedidoconsumible")
                .whereEqualTo("idPedido",pedido.idDoc)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                        @Nullable FirebaseFirestoreException e)
                    {

                        consumibles.clear();

                        Log.v("db","size "+queryDocumentSnapshots.size());
                        for(QueryDocumentSnapshot doc:queryDocumentSnapshots)
                        {
                            Log.v("db ",doc.getId());

                            Consumible consumible=new Consumible();
                            consumible.idDoc=doc.getId();
                            consumible.precio=doc.getDouble("precio");
                            consumible.nombre=doc.getString("nombre");
                            consumible.cantidad=doc.getLong("cantidad");

                            consumibles.add(consumible);

                        }
                        adapter.notifyDataSetChanged();

                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();

        if(dbListener!=null)
            dbListener.remove();
    }


}
