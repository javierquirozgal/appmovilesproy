package com.devper.registropedido.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.devper.registropedido.App;
import com.devper.registropedido.activities.Main;
import com.devper.registropedido.R;
import com.devper.registropedido.adapters.ConsumiblesAdapter;
import com.devper.registropedido.models.Consumible;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class FragmentConsumibles extends Fragment implements View.OnClickListener{

    View mViewRoot;
    RecyclerView rvLista;
    ArrayList<Consumible> consumibles;
    ConsumiblesAdapter adapter;
    ImageButton btnAgregar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewRoot=inflater.inflate(R.layout.fragment_consumibles,container,false);
        rvLista=mViewRoot.findViewById(R.id.rvLista);

        btnAgregar=mViewRoot.findViewById(R.id.btnAgregar);
        btnAgregar.setOnClickListener(this);
        ((Main)getActivity()).NuevoPedidoPrecioTotal=0;
        return mViewRoot;
    }

    @Override
    public void onClick(View v) {


        for(Consumible consumible:consumibles)
        {
            if(consumible.cantidad>0)
            {
                ((Main)getActivity()).NuevoPedidoSelecionados.add(consumible);
                ((Main)getActivity()).NuevoPedidoPrecioTotal+=consumible.precio*consumible.cantidad;
            }
        }
        getActivity().onBackPressed();
        //((Main)getActivity()).mostrarAgregarPedido();
    }

    ListenerRegistration dbListener;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        consumibles=new ArrayList<>();

        adapter=new ConsumiblesAdapter(consumibles,(Main)getActivity());
        LinearLayoutManager manager=new LinearLayoutManager(getContext());
        rvLista.setLayoutManager(manager);

        rvLista.setAdapter(adapter);


    }

    @Override
    public void onResume() {
        super.onResume();

        dbListener= App.class.cast(getActivity().getApplication()).getFbFirestore().collection("consumible")
                //.whereEqualTo("nick","javier")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                        @Nullable FirebaseFirestoreException e)
                    {

                        //queryDocumentSnapshots.size()
                        consumibles.clear();

                        Log.v("db","size "+queryDocumentSnapshots.size());
                        for(QueryDocumentSnapshot doc:queryDocumentSnapshots)
                        {
                            Log.v("db ",doc.getId());

                            Consumible consumible=new Consumible();
                            consumible.idDoc=doc.getId();
                            consumible.precio=doc.getDouble("precio");
                            consumible.nombre=doc.getString("nombre");

                            consumibles.add(consumible);

                        }
                        adapter.notifyDataSetChanged();

                    }
                });
    }

    @Override
    public void onPause() {
        super.onPause();

        if(dbListener!=null)
            dbListener.remove();
    }


}
