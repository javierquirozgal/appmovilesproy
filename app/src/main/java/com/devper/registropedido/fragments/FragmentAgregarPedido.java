package com.devper.registropedido.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.devper.registropedido.App;
import com.devper.registropedido.Helper;
import com.devper.registropedido.activities.Main;
import com.devper.registropedido.R;
import com.devper.registropedido.adapters.NuevoPedidoAdapter;
import com.devper.registropedido.models.AgregarConsumibles;
import com.devper.registropedido.models.AgregarMeza;
import com.devper.registropedido.models.Consumible;
import com.devper.registropedido.models.Pedido;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.Date;

public class FragmentAgregarPedido extends Fragment implements View.OnClickListener{


    View mViewRoot;
    ImageButton btnGuardar;
    RecyclerView rvLista;
    ArrayList<Object> elementos=new ArrayList<>();
    NuevoPedidoAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mViewRoot=inflater.inflate(R.layout.fragment_agregar_pedido,container,false);
        rvLista=mViewRoot.findViewById(R.id.rvLista);

        btnGuardar=mViewRoot.findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(this);

        return mViewRoot;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter=new NuevoPedidoAdapter(elementos,(Main) getActivity());
        LinearLayoutManager manager=new LinearLayoutManager(getContext());
        rvLista.setLayoutManager(manager);
        rvLista.setAdapter(adapter);

    }


    @Override
    public void onResume() {
        super.onResume();


        refrescar();
    }

    private void refrescar()
    {
        elementos.clear();
        AgregarMeza primerelemento=new AgregarMeza();
        if(((Main)getActivity()).mezaselecionada!=null)
        {
            primerelemento.meza_nombre=((Main)getActivity()).mezaselecionada.nombre;
        }
        elementos.add(primerelemento);
        AgregarConsumibles segundoelemento=new AgregarConsumibles();
        if(((Main)getActivity()).NuevoPedidoPrecioTotal>0)
        {
            segundoelemento.precio=((Main)getActivity()).NuevoPedidoPrecioTotal;
        }
        elementos.add(segundoelemento);

        if(((Main)getActivity()).NuevoPedidoSelecionados!=null && ((Main)getActivity()).NuevoPedidoSelecionados.size()>0)
        {
            elementos.addAll(((Main)getActivity()).NuevoPedidoSelecionados);
        }
        adapter.notifyDataSetChanged();
    }



    ProgressDialog progreso;
    @Override
    public void onClick(View v) {



        if(v.getId()==btnGuardar.getId())
        {
            if(((Main)getActivity()).NuevoPedidoSelecionados!=null && ((Main)getActivity()).NuevoPedidoSelecionados.size()>0 && ((Main)getActivity()).mezaselecionada!=null)
            {

                progreso=new ProgressDialog(getActivity().getWindow().getContext());
                progreso.setTitle("Registro");
                progreso.setMessage("Registrando pedido");
                progreso.setCancelable(false);
                progreso.show();

                final Pedido pedido=new Pedido();
                pedido.precio=((Main)getActivity()).NuevoPedidoPrecioTotal;
                pedido.mozo_nombre="xxxxx";
                pedido.meza_id=((Main)getActivity()).mezaselecionada.idMeza;
                pedido.meza_nombre=((Main)getActivity()).mezaselecionada.nombre;
                pedido.mozo_id=App.class.cast(getActivity().getApplication()).getFbAuth().getUid();
                pedido.fecha=new Date();
                StringBuilder sb=new StringBuilder();
                for(Consumible consumible:((Main)getActivity()).NuevoPedidoSelecionados)
                {
                    sb.append(consumible.cantidad+" "+consumible.nombre);
                    sb.append("\n");
                }
                pedido.detalle=sb.toString();

                App.class.cast(getActivity().getApplication()).getFbFirestore()
                        .collection("pedido")
                        .add(pedido)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Toast.makeText(getContext(),"Pedido ingresado",Toast.LENGTH_SHORT).show();

                                WriteBatch wb=App.class.cast(getActivity().getApplication()).getFbFirestore().batch();
                                if(((Main)getActivity()).NuevoPedidoSelecionados!=null && ((Main)getActivity()).NuevoPedidoSelecionados.size()>0)
                                {
                                    for(Consumible consumible:((Main)getActivity()).NuevoPedidoSelecionados)
                                    {
                                        consumible.idPedido=documentReference.getId();
                                        DocumentReference referencia=App.class.cast(getActivity().getApplication()).getFbFirestore().collection("pedidoconsumible").document(String.valueOf(Helper.getNewId()));
                                        wb.set(referencia,consumible);
                                    }
                                }
                                wb.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                        ((Main)getActivity()).mezaselecionada=null;
                                        ((Main)getActivity()).NuevoPedidoPrecioTotal=0;
                                        ((Main)getActivity()).NuevoPedidoSelecionados.clear();
                                        ((Main)getActivity()).cantidades.clear();
                                        refrescar();
                                        Toast.makeText(getContext(),"Pedido completo",Toast.LENGTH_SHORT).show();
                                        progreso.cancel();

                                        getActivity().onBackPressed();

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(getContext(),"Pedido datos fallido",Toast.LENGTH_SHORT).show();
                                        progreso.cancel();
                                    }
                                });


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progreso.cancel();
                        Toast.makeText(getContext(),"Pedido ingreso fallido",Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progreso.cancel();
                    }
                });
            }













        }




    }
}
