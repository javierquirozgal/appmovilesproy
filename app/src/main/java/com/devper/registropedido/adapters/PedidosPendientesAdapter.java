package com.devper.registropedido.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devper.registropedido.activities.Main;
import com.devper.registropedido.R;
import com.devper.registropedido.models.Pedido;

import java.util.ArrayList;

public class PedidosPendientesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public ArrayList<Pedido> pedidos;
    Main activity;
    public PedidosPendientesAdapter(ArrayList<Pedido> pedidos,Main activity)
    {
        this.pedidos=pedidos;
        this.activity=activity;
    }

    //aqui se crea la vista xml para cada elemento
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=LayoutInflater.from(parent.getContext()).inflate(R.layout.pedido_item,parent,false);
        PedidoItem item=new PedidoItem(itemView);
        return item;
    }

    //aqui se le pone la informacion a cada vista xml generada en el metodo anterior
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((PedidoItem)holder).bindView(pedidos.get(position));
    }

    @Override
    public int getItemCount() {
        return pedidos.size();
    }

    public class PedidoItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvNombre;
        TextView tvCosto;
        TextView tvDetalle;
        public PedidoItem(View itemView) {
            super(itemView);
            tvNombre=itemView.findViewById(R.id.tvName);
            tvCosto=itemView.findViewById(R.id.tvCost);
            tvDetalle=itemView.findViewById(R.id.tvDetalle);
        }

        public void bindView(Pedido pedido){

            itemView.setOnClickListener(this);
            tvNombre.setText(pedido.meza_nombre);
            tvCosto.setText(""+pedido.precio);
            tvDetalle.setText(pedido.detalle);
        }

        @Override
        public void onClick(View v) {

            activity.mostrarConsumiblesDePedido(pedidos.get(getLayoutPosition()));
        }
    }
}
