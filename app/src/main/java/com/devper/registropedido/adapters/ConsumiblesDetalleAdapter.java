package com.devper.registropedido.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.devper.registropedido.R;
import com.devper.registropedido.models.Consumible;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ConsumiblesDetalleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public ArrayList<Consumible> pedidos;
    public ConsumiblesDetalleAdapter(ArrayList<Consumible> pedidos)
    {
        this.pedidos=pedidos;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=LayoutInflater.from(parent.getContext()).inflate(R.layout.consumible_item_pedido, parent,false);
        ConsumibleItem item=new ConsumibleItem(itemView);
        return item;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((ConsumibleItem)holder).bindView(pedidos.get(position));
    }

    @Override
    public int getItemCount() {
        return pedidos.size();
    }

    public class ConsumibleItem extends RecyclerView.ViewHolder  {

        TextView tvNombre;
        TextView tvCosto;
        TextView tvCantidad;


        Consumible consumible;
        public ConsumibleItem(View itemView) {
            super(itemView);
            tvCantidad=itemView.findViewById(R.id.tvCantidad);
            tvNombre=itemView.findViewById(R.id.tvName);
            tvCosto=itemView.findViewById(R.id.tvCost);

        }

        public void bindView(Consumible consumible){

            this.consumible=consumible;

            tvNombre.setText(consumible.nombre);
            tvCosto.setText("PRECIO "+consumible.precio);


            tvCantidad.setText("CANTIDAD : "+consumible.cantidad);
            //tvNombre.setText(pedido.tableName);
            //tvCosto.setText(""+pedido.totalCost);
        }


    }
}
