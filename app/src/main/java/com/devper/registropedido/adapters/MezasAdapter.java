package com.devper.registropedido.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devper.registropedido.activities.Main;
import com.devper.registropedido.R;
import com.devper.registropedido.models.Meza;

import java.util.ArrayList;

public class MezasAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public ArrayList<Meza> mezas;
    Main activity;
    public MezasAdapter(ArrayList<Meza> mezas,Main activity)
    {
        this.mezas=mezas;
        this.activity=activity;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=LayoutInflater.from(parent.getContext()).inflate(R.layout.meza_item,parent,false);
        PedidoItem item=new PedidoItem(itemView);
        return item;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((PedidoItem)holder).bindView(mezas.get(position));
    }

    @Override
    public int getItemCount() {
        return mezas.size();
    }

    public class PedidoItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvNombre;
        TextView tvCapacidad;
        public PedidoItem(View itemView) {
            super(itemView);
            tvNombre=itemView.findViewById(R.id.tvNombre);
            tvCapacidad=itemView.findViewById(R.id.tvCapacidad);
            itemView.setOnClickListener(this);
        }

        public void bindView(Meza meza){

            tvNombre.setText(meza.nombre);
            tvCapacidad.setText(""+meza.capacidad);
        }

        @Override
        public void onClick(View v) {

            activity.mezaselecionada=mezas.get(getLayoutPosition());
            activity.onBackPressed();
        }
    }
}
