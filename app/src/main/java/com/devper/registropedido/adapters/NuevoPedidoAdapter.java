package com.devper.registropedido.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devper.registropedido.activities.Main;
import com.devper.registropedido.R;
import com.devper.registropedido.models.AgregarConsumibles;
import com.devper.registropedido.models.AgregarMeza;
import com.devper.registropedido.models.Consumible;

import java.util.ArrayList;

public class NuevoPedidoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public ArrayList<Object> elementos;
    Main activity;
    public NuevoPedidoAdapter(ArrayList<Object> elementos, Main activity)
    {
        this.elementos=elementos;
        this.activity=activity;
    }



    @Override
    public int getItemViewType(int position) {
        int tipo=0;

        if(elementos.get(position) instanceof AgregarMeza)
        {
            tipo=1;
        }
        else if(elementos.get(position) instanceof AgregarConsumibles)
        {
            tipo=2;
        }
        else if(elementos.get(position) instanceof Consumible)
        {
            tipo=3;
        }
        Log.v("JQG","pos "+tipo);
        return tipo;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder=null;
        View itemView;
        switch (viewType)
        {
            case 1:
                itemView=LayoutInflater.from(parent.getContext()).inflate(R.layout.agregarmeza_item,parent,false);
                viewHolder=new AgregarMezaHolder(itemView);
                break;
            case 2:
                itemView=LayoutInflater.from(parent.getContext()).inflate(R.layout.agregarconsumibles_item,parent,false);
                viewHolder=new AgregarConsumiblesHolder(itemView);
                break;
            case 3:
                itemView=LayoutInflater.from(parent.getContext()).inflate(R.layout.consumible_item_pedido,parent,false);
                viewHolder=new ConsumibleItem(itemView);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position))
        {
            case 1:
                ((AgregarMezaHolder)holder).bind((AgregarMeza) elementos.get(position));
                break;
            case 2:
                ((AgregarConsumiblesHolder)holder).bind((AgregarConsumibles) elementos.get(position));
                break;
            case 3:
                ((ConsumibleItem)holder).bindView((Consumible) elementos.get(position));
                break;
        }

    }

    @Override
    public int getItemCount() {
        return elementos.size();
    }

    public class AgregarMezaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTexto;
        public AgregarMezaHolder(View view)
        {
            super(view);
            tvTexto=view.findViewById(R.id.tvTexto);
        }
        public void bind(AgregarMeza elemento)
        {
            itemView.setOnClickListener(this);
            if(elemento.meza_nombre!=null)
            tvTexto.setText(elemento.meza_nombre);
        }

        @Override
        public void onClick(View v) {
            activity.mostrarMezas();
        }
    }
    public class AgregarConsumiblesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTexto;
        public AgregarConsumiblesHolder(View view)
        {
            super(view);
            tvTexto=view.findViewById(R.id.tvTexto);
        }
        public void bind(AgregarConsumibles elemento)
        {
            if(itemView!=null)
            {
                itemView.setOnClickListener(this);
                tvTexto.setText("Total :"+elemento.precio);
            }

        }

        @Override
        public void onClick(View v) {

            activity.NuevoPedidoSelecionados.clear();
            activity.mostrarConsumibles();
        }
    }

    public class ConsumibleItem extends RecyclerView.ViewHolder {

        TextView tvNombre;
        TextView tvCosto;
        TextView tvCantidad;

        Consumible consumible;
        public ConsumibleItem(View itemView) {
            super(itemView);
            tvCantidad=itemView.findViewById(R.id.tvCantidad);
            tvNombre=itemView.findViewById(R.id.tvName);
            tvCosto=itemView.findViewById(R.id.tvCost);

        }

        public void bindView(Consumible consumible){

            this.consumible=consumible;
            tvNombre.setText(consumible.nombre);
            tvCosto.setText("PRECIO "+consumible.precio);
            tvCantidad.setText("CANTIDAD : "+consumible.cantidad);
            //tvNombre.setText(pedido.tableName);
            //tvCosto.setText(""+pedido.totalCost);
        }


    }
}
