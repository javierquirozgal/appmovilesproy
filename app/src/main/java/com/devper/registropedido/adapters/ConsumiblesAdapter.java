package com.devper.registropedido.adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.devper.registropedido.R;
import com.devper.registropedido.activities.Main;
import com.devper.registropedido.models.Consumible;
import com.devper.registropedido.models.Pedido;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ConsumiblesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public ArrayList<Consumible> pedidos;
    Main activity;
    public ConsumiblesAdapter(ArrayList<Consumible> pedidos,Main activity)
    {
        this.pedidos=pedidos;
        this.activity=activity;

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=LayoutInflater.from(parent.getContext()).inflate(R.layout.consumible_item,parent,false);
        ConsumibleItem item=new ConsumibleItem(itemView);
        return item;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((ConsumibleItem)holder).bindView(pedidos.get(position));
    }

    @Override
    public int getItemCount() {
        return pedidos.size();
    }

    public class ConsumibleItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvNombre;
        TextView tvCosto;
        TextView tvCantidad;
        ImageButton btnMas;
        ImageButton btnMenos;


        Consumible consumible;
        public ConsumibleItem(View itemView) {
            super(itemView);
            tvCantidad=itemView.findViewById(R.id.tvCantidad);
            tvNombre=itemView.findViewById(R.id.tvName);
            tvCosto=itemView.findViewById(R.id.tvCost);
            btnMas=itemView.findViewById(R.id.btnMas);
            btnMenos=itemView.findViewById(R.id.btnMenos);

        }

        public void bindView(Consumible consumible){

            this.consumible=consumible;
            btnMas.setOnClickListener(this);
            btnMenos.setOnClickListener(this);

            tvNombre.setText(consumible.nombre);
            tvCosto.setText("PRECIO "+consumible.precio);

            if(consumible.cantidad==0 && activity.cantidades.containsKey(consumible.idDoc))
            {
                if(activity.cantidades.get(consumible.idDoc)!=0)
                {
                    consumible.cantidad=activity.cantidades.get(consumible.idDoc);
                }
            }
            tvCantidad.setText("CANTIDAD : "+consumible.cantidad);
            //tvNombre.setText(pedido.tableName);
            //tvCosto.setText(""+pedido.totalCost);
        }

        @Override
        public void onClick(View v) {

            if(v.getId()==btnMas.getId())
            {
                consumible.cantidad++;
                activity.cantidades.put(consumible.idDoc,(int)consumible.cantidad);

                notifyItemChanged(getLayoutPosition());
            }
            else if(v.getId()==btnMenos.getId())
            {
                if(consumible.cantidad>0)
                {
                    consumible.cantidad--;
                    activity.cantidades.put(consumible.idDoc,(int)consumible.cantidad);
                    notifyItemChanged(getLayoutPosition());
                }

            }

        }
    }
}
