package com.devper.registropedido.models;

import com.google.firebase.firestore.Exclude;

public class Meza {
    @Exclude
    public String idMeza;

    public String nombre;
    public int capacidad;
    public String area;
    public String sucursal;
}
