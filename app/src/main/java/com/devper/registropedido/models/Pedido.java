package com.devper.registropedido.models;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Exclude;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class Pedido {

    @Exclude
    public String idDoc;
    public String meza_id;
    public String meza_nombre;
    public String mozo_id;
    public String mozo_nombre;
    public double precio;
    public String detalle;
    public Date fecha;

}
