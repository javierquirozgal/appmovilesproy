package com.devper.registropedido.models;

import com.google.firebase.firestore.Exclude;

public class Consumible {
    @Exclude
    public String idDoc;
    public String idPedido;
    public String nombre;
    public double precio;
    public String tipo;
    public double fechacreacion;
    public long cantidad;

}
