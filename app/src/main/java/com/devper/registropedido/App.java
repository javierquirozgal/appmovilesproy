package com.devper.registropedido;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

public class App extends MultiDexApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    public void onCreate() {
        super.onCreate();
    }

    private FirebaseAuth fbAuth;
    public FirebaseAuth getFbAuth()
    {
        if(fbAuth==null)
        {
            fbAuth=FirebaseAuth.getInstance();
        }
        return fbAuth;
    }

    FirebaseFirestore db ;
    public FirebaseFirestore getFbFirestore()
    {
        if(db==null)
        db = FirebaseFirestore.getInstance();
        return db;
    }
}
