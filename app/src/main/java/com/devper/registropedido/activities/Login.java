package com.devper.registropedido.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.devper.registropedido.App;
import com.devper.registropedido.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

public class Login extends AppCompatActivity implements View.OnClickListener {

    EditText etUser,etPass;
    ImageButton btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        etUser=findViewById(R.id.etUser);
        etPass=findViewById(R.id.etPass);
        btnLogin=findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(this);
    }


    String user,pass;

    // se ejecuta este codigo al darle click al imagebutton
    @Override
    public void onClick(View v) {
        user=etUser.getText().toString();
        pass=etPass.getText().toString();

        //validadmos los valores de usuario y password
        if(user.length()>0 && pass.length()>0)
        {

            //inicia sesion con usuario y password
            App.class.cast(getApplication()).getFbAuth().signInWithEmailAndPassword(user,pass)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete( Task<AuthResult> task) {


                            //si la tarea satisfactoria
                            if (task.isSuccessful()) {
                                Intent i=new Intent(getApplicationContext(),Main.class);
                                startActivity(i);
                                finish();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),"Ocurrio un error",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }


    }

}
