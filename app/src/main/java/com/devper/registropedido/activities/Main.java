package com.devper.registropedido.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.devper.registropedido.R;
import com.devper.registropedido.fragments.FragmentAgregarPedido;
import com.devper.registropedido.fragments.FragmentConsumibles;
import com.devper.registropedido.fragments.FragmentDetalleConsumibles;
import com.devper.registropedido.fragments.FragmentMezas;
import com.devper.registropedido.fragments.FragmentPendientes;
import com.devper.registropedido.models.Consumible;
import com.devper.registropedido.models.Meza;
import com.devper.registropedido.models.Pedido;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main extends AppCompatActivity {


    public ArrayList<Consumible> NuevoPedidoSelecionados=new ArrayList<>();
    public double NuevoPedidoPrecioTotal=0;
    public Meza mezaselecionada;
    public  Map<String,Integer> cantidades=new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer,new FragmentPendientes())
                .commit();

    }

    public void mostrarAgregarPedido()
    {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer,new FragmentAgregarPedido())
                .addToBackStack("agregar")
                .commit();
    }

    public void mostrarConsumibles()
    {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer,new FragmentConsumibles())
                .addToBackStack("consumibles")
                .commit();
    }

    public void mostrarMezas()
    {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer,new FragmentMezas())
                .addToBackStack("mezas")
                .commit();
    }

    public void mostrarConsumiblesDePedido(Pedido pedido)
    {
        FragmentDetalleConsumibles detalle=new FragmentDetalleConsumibles();
        detalle.pedido=pedido;

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer,detalle)
                .addToBackStack("detalle")
                .commit();
    }
}
