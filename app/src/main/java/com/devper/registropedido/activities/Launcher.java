package com.devper.registropedido.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.devper.registropedido.App;
import com.devper.registropedido.R;
import com.google.firebase.auth.FirebaseAuth;

public class Launcher extends AppCompatActivity{

    FirebaseAuth.AuthStateListener fbAuthListener;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launcher);
        fbAuthListener=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                //si el usuario actual es diferente null
                if(firebaseAuth.getCurrentUser()!=null)
                {

                    //si es asi muestra el activity principal
                    Intent i=new Intent(getApplicationContext(),Main.class);
                    startActivity(i);
                    finish();
                }
                else{

                    //si no es asi, muestra el login
                    Intent i=new Intent(getApplicationContext(),Login.class);
                    startActivity(i);
                    finish();
                }
            }
        };
    }


    //cuando el activity(pantalla) es visible al usuario
    @Override
    protected void onResume() {
        super.onResume();

        //Uso de asynctask para  crear el efecto de la demora
        // 3 segundos con Thread.sleep()
        new AsyncTask<String,Integer,String>()
        {
            @Override
            protected String doInBackground(String... strings) {
                for(int k=0;k<2;k++)
                {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                //Aqui continua con lo que tiene que hacer
                //Empieza a escuchar si hay una sesion o alguien logueado
                App.class.cast(getApplication()).getFbAuth().addAuthStateListener(fbAuthListener);
            }
        }.execute("");

    }

    //cuando el activity(pantalla) se oculta del usuario
    @Override
    protected void onPause() {
        super.onPause();
        if(fbAuthListener!=null)
            App.class.cast(getApplication()).getFbAuth().removeAuthStateListener(fbAuthListener);

    }
}
